﻿namespace Maintenance.Data
{
    public class Vendor : BaseEntity
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string ContactPerson { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public string AttentionTo { get; set; }
        public string OtherInformation { get; set; }
        public VendorStatus Status { get; set; }
    }
}
