﻿using System.ComponentModel;

namespace Maintenance.Data
{
    public enum VendorStatus
    {
        Active,
        Inactive,
        [DisplayName("For Deletion")]
        ForDeletion

    }
}
