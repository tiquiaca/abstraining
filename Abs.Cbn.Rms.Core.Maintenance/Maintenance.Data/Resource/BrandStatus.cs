﻿namespace Maintenance.Data
{
    public enum BrandStatus
    {
        Active,
        Inactive
    }
}
