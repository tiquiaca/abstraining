﻿
namespace Maintenance.Data
{
    public class ClassLevel : BaseEntity
    {
        public int Number { get; set; }
        public string Name { get; set; }
        public int CompanyNameId { get; set; }
        public virtual Company CompanyName { get; set; }
    }
}
