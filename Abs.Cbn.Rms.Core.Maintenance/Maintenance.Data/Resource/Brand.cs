﻿
namespace Maintenance.Data
{
    public class Brand : BaseEntity
    {
        public string Name { get; set; }
        public BrandStatus Status { get; set; }
    }
}
