﻿namespace Maintenance.Data
{
    public class Company:BaseEntity
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
