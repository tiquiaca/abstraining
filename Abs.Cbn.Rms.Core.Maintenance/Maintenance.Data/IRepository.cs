﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Maintenance.Data
{
    public interface IRepository<T> where T : BaseEntity
    {
        IEnumerable<T> GetAll();
        PagedResult<T> GetByPage(int page, int pageSize);
        T Get(int id);
        T GetNoTracking(int id);
        Task AddAsync(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Remove(T entity);
        Task SaveChangesAsync();
    }
}

