﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Maintenance.Data
{
    public class BaseEntity
    {
        public int Id { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime Modified { get; set; }
    }
}
