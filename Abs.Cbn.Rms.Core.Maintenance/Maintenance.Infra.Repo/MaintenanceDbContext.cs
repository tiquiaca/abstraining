﻿using Maintenance.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Maintenance.Infra.Repo
{
    public class MaintenanceDbContext : DbContext
    {
        public MaintenanceDbContext(DbContextOptions options)
         : base(options)
        {
        }
        #region Company
        public DbSet<Company> Company { get; set; }
        #endregion
        #region Resource
        public DbSet<Brand> Brand { get; set; }
        public DbSet<ClassLevel> ClassLevel { get; set; }
        #endregion  
        #region Vendor
        public DbSet<Vendor> Vendor { get; set; }
        #endregion
     
    }
}
