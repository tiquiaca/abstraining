﻿using Maintenance.Data;
using Maintenance.Extensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maintenance.Infra.Repo
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly MaintenanceDbContext _context;
        private readonly DbSet<T> _entities;

        public Repository(MaintenanceDbContext context)
        {
            _context = context;
            _entities = context.Set<T>();
        }

        public async Task AddAsync(T entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            await _entities.AddAsync(entity);
        }

        public void Delete(T entity) => _entities.Remove(entity);

        public T Get(int id) => _entities.SingleOrDefault(s => s.Id == id);

        public IEnumerable<T> GetAll() => _entities.AsEnumerable();

        public PagedResult<T> GetByPage(int page, int pageSize) => _entities.AsQueryable().GetPaged(page, pageSize);

        public T GetNoTracking(int id)=> _entities.AsNoTracking().SingleOrDefault(s => s.Id == id);

        public void Remove(T entity) => _entities.Remove(entity);
        
        public async Task SaveChangesAsync() => await _context.SaveChangesAsync();
        
        public void Update(T entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");

            _entities.Update(entity);
        }
    }
}
