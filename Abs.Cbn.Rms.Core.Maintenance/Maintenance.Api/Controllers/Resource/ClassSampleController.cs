﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Maintenance.Data;
using Maintenance.Data.Resource;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Maintenance.Api.Controllers
{
    [Route("api/[controller]")]
    public class ClassSampleController : Controller
    {

        private readonly IRepository<ClassSample> _repository;

        public ClassSampleController(IRepository<ClassSample> repository)
        {
            _repository = repository;
        }

        // GET All: api/<controller>
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_repository.GetAll());
            }
            catch (Exception)
            {
                //Logger here
                return StatusCode((int)HttpStatusCode.InternalServerError, "Error encountered");
            }
        }

        // GET By Page: api/<controller>
        [HttpGet("GetByPage")]
        public IActionResult GetByPage(int page, int pageSize)
        {
            try
            {
                return Ok(_repository.GetByPage(page, pageSize));
            }
            catch (Exception)
            {
                //Logger here
                return StatusCode((int)HttpStatusCode.InternalServerError, "Error encountered");
            }

        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
               
                return Ok(_repository.Get(id));
            }
            catch (Exception)
            {
                //Logger here
                return StatusCode((int)HttpStatusCode.InternalServerError, "Error encountered");
            }
            
        }

        // POST api/<controller>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ClassSample ClassSample)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);
                await _repository.AddAsync(ClassSample);
                await _repository.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                //Logger here
                return StatusCode((int)HttpStatusCode.InternalServerError, "Error encountered");
            }
         
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]ClassSample ClassSample)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);
                 _repository.Update(ClassSample);
                 _repository.SaveChangesAsync();
                return Ok();
            }
            catch (Exception)
            {
                //Logger here
                return StatusCode((int)HttpStatusCode.InternalServerError, "Error encountered");
            }
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                var ClassSample= _repository.GetNoTracking(id);
                _repository.Remove(ClassSample);
                _repository.SaveChangesAsync();
                return Ok();
            }
            catch (Exception)
            {
                //Logger here
                return StatusCode((int)HttpStatusCode.InternalServerError, "Error encountered");
            }
        }
    }
}
