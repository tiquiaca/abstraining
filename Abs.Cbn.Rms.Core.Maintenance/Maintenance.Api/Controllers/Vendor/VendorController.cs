﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Maintenance.Data;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Maintenance.Api.Controllers
{
    [Route("api/[controller]")]
    public class VendorController : Controller
    {

        private readonly IRepository<Vendor> _repository;

        public VendorController(IRepository<Vendor> repository)
        {
            _repository = repository;
        }

        // GET All: api/<controller>
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_repository.GetAll());
            }
            catch (Exception)
            {
                //Logger here
                return StatusCode((int)HttpStatusCode.InternalServerError, "Error encountered");
            }
        }

        // GET By Page: api/<controller>
        [HttpGet("GetByPage")]
        public IActionResult GetByPage(int page, int pageSize)
        {
            try
            {
                return Ok(_repository.GetByPage(page, pageSize));
            }
            catch (Exception)
            {
                //Logger here
                return StatusCode((int)HttpStatusCode.InternalServerError, "Error encountered");
            }

        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
               
                return Ok(_repository.Get(id));
            }
            catch (Exception)
            {
                //Logger here
                return StatusCode((int)HttpStatusCode.InternalServerError, "Error encountered");
            }
            
        }

        // POST api/<controller>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Vendor vendor)
        {
            try
            {
                vendor.Modified = DateTime.Now;
                vendor.ModifiedBy = "Current user";
                if (!ModelState.IsValid) return BadRequest(ModelState);
                await _repository.AddAsync(vendor);
                await _repository.SaveChangesAsync();
                return Ok();
            }
            catch (Exception)
            {
                //Logger here
                return StatusCode((int)HttpStatusCode.InternalServerError, "Error encountered");
            }
         
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Vendor vendor)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);
                 _repository.Update(vendor);
                 _repository.SaveChangesAsync();
                return Ok();
            }
            catch (Exception)
            {
                //Logger here
                return StatusCode((int)HttpStatusCode.InternalServerError, "Error encountered");
            }
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
               
                var vendor = new Vendor { Id = id };
                _repository.Remove(vendor);
                _repository.SaveChangesAsync();
                return Ok();
            }
            catch (Exception)
            {
                //Logger here
                return StatusCode((int)HttpStatusCode.InternalServerError, "Error encountered");
            }
        }
    }
}
