import { Component, OnInit } from '@angular/core';
import { Vendor } from '../models/vendor';
import { VendorService } from '../services/vendor-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vendor-list',
  templateUrl: './vendor-list.component.html',
  styleUrls: ['./vendor-list.component.css']
})


export class VendorListComponent implements OnInit {
  isLoader=false;
  isError=false;
  vendors: Vendor[]=[];
  errorMessage: any;
  constructor(private service:VendorService,
    private router: Router) { }

  ngOnInit() {
    this.getVendors();
  }

  getVendors(){
    this.isLoader=true;
    this.isError=false;
    this.service.getVendors()
    .subscribe( data => {
      this.vendors = data;
      this.isLoader=false;
    },
    err => {console.error('Error encountered: ' + JSON.stringify(err));
    this.isLoader=false;
    this.isError=true;},
    () => console.log('Completed'));
  }

  deleteVendor(vendor: Vendor): void {
    this.service.deleteVendor(vendor.id)
      .subscribe( data =>{ 
      this.vendors = this.vendors.filter(u => u !== vendor);
      });
 
}

}

