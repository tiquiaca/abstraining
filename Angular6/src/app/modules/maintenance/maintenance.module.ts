import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorListComponent } from './vendor-list/vendor-list.component';
import { MaintenanceHomeComponent } from './maintenance-home/maintenance-home.component';
import { RouterModule, Routes } from '../../../../node_modules/@angular/router';
import { VendorAddComponent } from './vendor-add/vendor-add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VendorService } from './services/vendor-service';

const childRoutes: Routes = [
  {
    path: '',
    component: MaintenanceHomeComponent
  },
  {
      path: 'vendorlist',
      component: VendorListComponent
  },
  {
    path: 'vendoradd',
    component: VendorAddComponent
}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(childRoutes),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    VendorListComponent,
    MaintenanceHomeComponent,
    VendorAddComponent
  ],
  providers:[
    VendorService
  ]
})
export class MaintenanceModule { }
