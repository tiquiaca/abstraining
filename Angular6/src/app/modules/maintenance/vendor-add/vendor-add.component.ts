import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { VendorService } from '../services/vendor-service';

@Component({
  selector: 'app-vendor-add',
  templateUrl: './vendor-add.component.html',
  styleUrls: ['./vendor-add.component.css']
})
export class VendorAddComponent implements OnInit {
  vendorForm: FormGroup; 
  submitted = false;
  isError=false;

  constructor(private fb: FormBuilder, 
    private service: VendorService,
    private router: Router) { 
    
  }

get f() { return this.vendorForm.controls; }

  ngOnInit() {
    this.vendorForm = this.fb.group({
      id: 0,
      code: ['',Validators.required],
      name: ['',[Validators.required,Validators.minLength(5)]],
      contactPerson: '',
      address: '',
      contactNo: '',
      attentionTo: '',
      otherInformation: '',
      status: 0,
    });
  }


  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.vendorForm.invalid) {
    //  this.submitted = false;
        return;
    
    }
 
 
    this.service.createVendor(this.vendorForm.value)
      .subscribe( data => {
        this.router.navigate(['maintenance/vendorlist']);
      },
    err => {console.error('Error encountered: ' + JSON.stringify(err));
    this.isError=true;
    this.submitted = false;
  },
    () => console.log('Added Vendor'));
  }



}

