import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Vendor } from '../models/vendor';


@Injectable()
export class VendorService {
  constructor(private http: HttpClient) { }
  baseUrl: string = 'http://localhost:5000/api/vendor/';

  getVendors() {

    return this.http.get<Vendor[]>(this.baseUrl);
  }


  createVendor(vendor: Vendor) {
    return this.http.post(this.baseUrl, vendor);
  }

  getVendor(id: number) {
    return this.http.get<Vendor>(this.baseUrl + id);
  }


  updateVendor(vendor: Vendor) {
    return this.http.put(this.baseUrl + vendor.id, vendor);
  }

  deleteVendor(id: number) {
    return this.http.delete(this.baseUrl + id);
  }
}
