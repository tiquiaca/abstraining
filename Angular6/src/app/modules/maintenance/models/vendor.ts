//export interface IVendor {
  //  id: number;
  //  code: string;
  //  name: string;
  //  status: string;
  //  updatedBy: string;
  //  updatedDate: string;
 // }

  export class Vendor {
    id = 0;
    code = '';
    name = '';
    contactPerson='';
    address ='';
    contactNo='';
    attentionTo='';
    otherInformation='';
    status = 0;
    modifiedBy = '';
    modifiedDate = '';
  }