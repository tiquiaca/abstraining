import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';

const appRoutes: Routes = [
    { path: 'home', component: HomeComponent },
    {
        path: 'maintenance',
        data: { preload: true },
        loadChildren: './modules/maintenance/maintenance.module#MaintenanceModule'
    },
    { path: '', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes) 
    ],
    providers: [],
    exports: [ RouterModule ]
})
export class AppRoutingModule { }
